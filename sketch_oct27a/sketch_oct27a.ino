#include "hardware_manager.h"
#include "movingAvg.h"

enum states {
  PREFLIGHT,
  ASCENT,
  DESCENT,
  POSTFLIGHT
};

// Creates an instance of the SensorData struct
SensorData_t sensorData;

// Start in PREFLIGHT
int state = PREFLIGHT;

// Create a moving average to store previous 10 pressure values
movingAvg pressure(10);

void setup() {
  Serial.begin(9600);
  HM_Init(sensorData);
  pressure.begin();
}


bool transitionCondition() {
  return true;
}

uint32_t accelThresholdTrueSince = millis();

// max altitude occurs at min pressure
int32_t minPressure = 1 * 10000; 
uint32_t prevPrint = millis();
// Time between prints
#define PRINT_DELAY 500
void loop() {
  HM_ReadSensorData(sensorData);
  // The moving average library only uses ints
  // So we multiply our float by 10000 then cast as int
  pressure.reading((int32_t)(sensorData.pressure*10000));

  int32_t newPres = pressure.getAvg();
  if (newPres < minPressure) {
    minPressure = newPres;
  }

  if (millis() - prevPrint > PRINT_DELAY) {
    prevPrint = millis();
    Serial.print("State: ");
    Serial.println(state);
    Serial.print("NewPres: ");
    Serial.println(newPres);
    Serial.print("Accz: ");
    Serial.println(sensorData.accel_z);
  }
  switch(state) {
    case PREFLIGHT:
      // If our accel isn't what we want, reset the timer
      if (!(sensorData.accel_z > 10)) {
        accelThresholdTrueSince = millis();
      }
      // If we get here, our accel has been at the threshold for 300ms
      // So we should transition!
      if (millis() - accelThresholdTrueSince > 1000) {
        state = ASCENT;
      }
      // Code for PREFLIGHT
      break;
    case ASCENT:
      // Higher pressure = Going down
      // This means we are past apogee - go to descent
      if (newPres > minPressure) {
        state = DESCENT;
      }
      // Code for ASCENT
      break;
    case DESCENT:
      // TODO: Implement descent transition to postflight
      if (transitionCondition()) {
        state = POSTFLIGHT;
      }
      // Code for DESCENT
      break;
    case POSTFLIGHT:
      // We can't leave POSTFLIGHT so no transition code needed here
      // Code for POSTFLIGHT
      break;
    default:
      // Shouldn't get here
      break;
  }
  // Delay for a bit
  delay(10);
}

